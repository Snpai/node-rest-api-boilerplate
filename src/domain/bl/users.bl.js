const moment = require('moment');

const userBl = () => {
  const scope = this;

  scope.get_users = async (users) => {
    // filter
    let newArray = users.filter((user) => user.is_active === true);
    // order
    newArray.sort((a, b) => {
      if (a.lastname > b.lastname) {
        return 1;
      }
      if (a.lastname < b.lastname) {
        return -1;
      }
      return 0;
    });
    newArray = users.map((item) => {
      const date = moment(item.birthday);
      // eslint-disable-next-line no-param-reassign
      item.age = date.fromNow(true);
      console.log(item);
      return item;
    });
    return newArray;
  }
  return scope;
  
}
module.exports = userBl;