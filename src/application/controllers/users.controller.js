/* eslint-disable no-else-return */
/* eslint-disable arrow-body-style */
/* eslint-disable no-param-reassign */
const fetch = require('node-fetch');
const moment = require('moment');
const config = require('../../../config');

const getData = () => {
  return fetch(config.api)
    .then((res) => res.json())
    .catch((e) => {
      e.status(500).send({
        message: 'error',
      });
    });
};
function usersFilter(users) {
  const newArray = users.filter((user) => user.is_active === true);
  return newArray;
}

function usersOrder(users) {
  users.sort((a, b) => {
    if (a.lastname > b.lastname) {
      return 1;
    }
    if (a.lastname < b.lastname) {
      return -1;
    }
    return 0;
  });
  return users;
}

function UsersWithAge(users) {
  const newArray = users.map((item) => {
    const date = moment(item.birthday);
    item.age = date.fromNow(true);
    // console.log(item);
    return item;
  });
  // console.log(newArray);
  return newArray;
}

const userController = {
  getUsers: async (req, res) => {
    const users = await getData();
    let usersArray = users.users;
    usersArray = await usersFilter(usersArray);
    usersArray = await usersOrder(usersArray);
    usersArray = await UsersWithAge(usersArray);
    if (usersArray.length > 0) {
      return res.json({
        usersArray,
      });
    } else {
      return res.status(500).send({
        message: 'error in the server',
      });
    }
  },
};

module.exports = userController;
