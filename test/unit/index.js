/* eslint-disable no-undef */
const { expect } = require('chai');
const userController = require('../../src/application/controllers/users.controller');

describe('/users', () => {
  it('Should if every ok', async () => {
    const mockReq = {};
    const mockResp = {
      json(obj) {
        return obj;
      },
    };
    const result = await userController.getUsers(mockReq, mockResp);
    expect(result).to.be.an('Object');
  });
});
