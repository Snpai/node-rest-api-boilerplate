# node-rest-api-boilerplate

👋 Hi, welcome to the test assessment.

Please take care of the requirements described below.
You have only 4 hours to complete the entire test. To save time to develop all the things, we're providing a simple boilerplate with all the basics to start to develop immediately. Don't forget to document all, we evaluate functionality, coding style, and presentation.

## First Steps
- Complete with your personal information the `package.json`.
- Read carefully the requirements
- Start to develop

## Requirements
Functionality: Get a list of active users

Scenario: any member, with available services

```
Given users exist in an external service
When executing a GET petition to the /users endpoint
Then I obtain all the active users ordered by last name
And each user has a field with his calculated age
```

Services to consume
- API Endpoint - https://2eja2nqth0.execute-api.us-east-1.amazonaws.com/api/users
- Swagger - https://app.swaggerhub.com/apis/jbrizio/ms-users-api/1.0.0

# Deliverables
- Repository on Github
- Swagger
- Test cases

## Stack used
- Node.js
- Express

# node-rest-api-boilerplate

node-rest-api-boilerplate it is an rest api that was made for an incluiT activity

## Initialize


to initialize the application
```bash
npm start
```

to initialize the application in development mode
```bash
npm run dev
```

to run the unit tests
```bash
npm test
```

## Usage
when you start the application, an element like the following will return
```python
{
  "users": [
    {
      "id": 0,
      "name": "string",
      "lastname": "string",
      "birthday": "2019-10-23",
      "is_active": true,
      "email": "string",
      "genre": "string"
    }
  ]
}
```


## License
[MIT](https://choosealicense.com/licenses/mit/)

## Advice
Please document all the things that you consider necessary. Follow the good practices followed in the course.

Good luck! 💪
